export default async function ({ store, $axios, $auth, redirect }, inject) {

    const api = $axios.create({
        baseURL: 'https://pokeapi.co/api/v2/',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        } 
    })

    api.interceptors.response.use(
        (response) => {
            return response
        },
        (error) => {
          
            const code = parseInt(error.response && error.response.status)
            
            if (code == 401) {
                store.commit('utils/toggleQuickMessage',  {text: error?.response?.data?.message, color: 'error'})
                // redirect('/login')
            }
            if (code == 403) {
                store.commit('utils/toggleQuickMessage',  {text: error?.response?.data?.message, color: 'error'})
                // redirect('/register')
            }
            if (code == 400) {
                store.commit('utils/toggleQuickMessage',  {text: error?.response?.data?.message, color: 'error'})
            }
            if (code == 404) {
                store.commit('utils/toggleQuickMessage',  {text: 'Request failed with status code 404.', color: 'warning'})
            }
            if (code == 500) {
                store.commit('utils/toggleQuickMessage',  {text: error?.response?.data?.message, color: 'error'})
            }
            return error.response
        }
    )

    inject('api', api)
}