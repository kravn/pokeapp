export const  state = () => ({
    pokemons: [],
    pokemon: {},
    evolution: {},
    characteristics: {},
    abilities: {},
    moves: {},
    types: {}
})

export const getters = {
    pokemons: (state) => { return state.pokemons },
    evolution: (state) => { return state.evolution },
    characteristics: (state) => { return state.characteristics },
    abilities: (state) => { return state.abilities },
    moves: (state) => { return state.pokemon.moves },
    pokemonType: (state) => { return state.pokemon.types },
    selectedPokemon: (state) => { return state.pokemon }
}

export const actions = {
    async getPokemons({commit}, params = {}) {
        let response = await this.$api.get(`pokemon?offset=${params.offset}&limit=${params.limit}`)
        if ( response.status === 200 ) {
            commit('onOkGetPokemons', response.data)
        }        
    },

    async getPokemon({commit}, id = '') {
        let response = await this.$api.get(`pokemon/${id}`)
        if ( response.status === 200 )
            commit('onOkGetPokemon', response.data)
    },

    async getEvolution({commit}, id = '') {
        let response = await this.$api.get(`evolution-chain/${id}`)
        if ( response.status === 200 )
            commit('onOkGetEvolution', response.data)
    },

    async getCharacteristics({commit}, id = '') {
        let response = await this.$api.get(`characteristic/${id}`)
        if ( response.status === 200 )
            commit('onOkGetCharacteristics', response.data)
    },

    async getAbilities({commit}, id = '') {
        let response = await this.$api.get(`ability/${id}`)
        if ( response.status === 200 )
            commit('onOkGetAbilities', response.data)
    },

    async getMoves({commit}, id = '') {
        let response = await this.$api.get(`move/${id}`)
        if ( response.status === 200 )
            commit('onOkGetMoves', response.data)
    },

    async getPokemonType({commit}, id = '') {
        let response = await this.$api.get(`type/${id}`)
        if ( response.status === 200 )
            commit('onOkGetPokemonType', response.data)
        return response.data
    }
}

export const  mutations = {
    onOkGetPokemons(state, data) {
        state.pokemons = data
    },
    onOkGetPokemon(state, data) {
        state.pokemon = data
    },
    onOkGetEvolution(state, data) {
        state.evolution = data
    },
    onOkGetCharacteristics(state, data) {
        state.characteristics = data
    },
    onOkGetAbilities(state, data) {
        state.abilities = data
    },
    onOkGetMoves(state, data) {
        state.moves = data
    },
    onOkGetPokemonType(state, data) {
        state.types = data
    }
    
}