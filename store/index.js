
// initial state
export const  state = () => ({
    loader: false,
    notification_settings: {
        snackbar: false,
        color: 'success',
        y: 'top',
        x: null,
        mode: 'multi-line',
        timeout: -1,
        text: 'Default Message!',
        timer: 5000
    },
})

// getters
export const  getters = {

}

// actions
export const actions = {

}
// mutations
export const  mutations = {
    toggleLoader(state) {
        state.loader = !state.loader
    },
    toggleQuickMessage(state) {
        if (state.notification_settings.snackbar) { state.notification_settings.snackbar = false }
        state.notification_settings.snackbar = !state.notification_settings.snackbar
        state.notification_settings.text = data.text
        state.notification_settings.color = data.color
    }
}