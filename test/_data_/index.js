import pokemon_list from './pokemon'

const data = {
    "pokemons": pokemon_list
}

export { data }