import Vue from 'vue'
import Vuetify from 'vuetify'
import PokemonList from '~/components/v2/gallery/PokemonList'
import { shallowMount } from '@vue/test-utils'

import * as source from '~/test/_data_'

Vue.use(Vuetify)

describe('PokemonList', () => {

    // const toggleLoader = jest.spyOn(PokemonList.methods, 'toggleLoader')
    // const getPokemons = jest.spyOn(PokemonList.methods, 'getPokemons')

    let wrapper = null
    afterEach( () => { wrapper.destroy() })    

    it('Fetches Pokemon List from API', () => {
        wrapper = shallowMount(PokemonList, {
            mounted() {},
            methods: {
                toggleLoader:jest.fn(),
                getPokemons:jest.fn()
            },
            computed: {
                searchResults() {
                    return []
                }
            }
        })        

        // wrapper.vm.getPokemons() = jest.fn()
        let pokemons = source

        expect(wrapper.vm).toBeTruthy()
    })
})
  